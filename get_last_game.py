# coding=utf-8
content = ""
previous_score = -100
with open("dataset/boards.history", "r") as fichier:
    while 1:
        readline = fichier.readline()
        if not readline: break
        board, score = readline.split(";")

        if not score == previous_score:
            content = ""
            previous_score = score

        content += board + ";" + str(score)

with open("dataset/last_game.history", "w") as fichier_w:
    fichier_w.write(content)