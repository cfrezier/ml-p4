# !/usr/local/anaconda3/bin/python
# -*- coding: utf-8 -*-

import os
import random

import keras
import numpy as np
from players.generic_player import Player


class GenericKeraXMovePlayer(Player):

    def __init__(self, name, me, quiet=False, deep=1):
        Player.__init__(self, name, me, quiet)
        self.model = None
        self.me = me
        self.deep = deep
        self.finisher = True
        self.defender = True

    def savingPath(self):
        if not os.path.exists("./models"):
            os.mkdir("./models")
        return "./models/model_{}_{}".format(self.name, self.me)

    def loadModel(self):
        keras.models.load_model(self.savingPath())

    def saveModel(self):
        self.model.save(self.savingPath())

    def respond_to(self, board):
        best_prob_to_win = -999999999.0
        best_x = 0

        for x in board.valid_moves():
            my_board = board if self.me == 1 else board.invert()

            prob_to_win = self.evaluate_next_moves_at_deep(my_board, x, self.deep - 1)

            self.quiet_print("Si joue en colonne {}, prob = [{}]".format(x, prob_to_win))

            if prob_to_win > best_prob_to_win:
                best_x = [x]
                best_prob_to_win = prob_to_win
            elif prob_to_win == best_prob_to_win:
                best_x.append(x)

        self.quiet_print("On a le choix entre {}".format(best_x))

        choosen_x = best_x[random.randint(0, len(best_x) - 1)]
        self.quiet_print("Alors jouons en {} (prob = {})".format(choosen_x, best_prob_to_win))
        return choosen_x

    def evaluate_next_moves_at_deep(self, board, previous_x, deep):
        result = 0.0
        after_move_board = board.simuler(1, previous_x)

        priority = float(100.0 / (self.deep - deep + 1))

        if self.finisher:
            # Profiter d'une victoire instantanée
            win = after_move_board.test_victoire()
            if not win == 0 and not win == 42:
                if win == (deep % 2 + 1):
                    return priority if win == (deep % 2 + 1) else -priority

        if self.defender:
            # Eviter une défaite instantanée
            win = board.simuler(2, previous_x).test_victoire()
            if not win == 0 and not win == 42:
                return -priority if win == (deep % 2 + 1) else priority

        for x in board.valid_moves():
            if deep > 0:
                this_move_result = self.evaluate_next_moves_at_deep(after_move_board.invert(), x, deep - 1)
                #self.quiet_print("prob for {}: {}".format(x, str(this_move_result)).rjust(40 - deep * 5))
                result += float(this_move_result if deep % 2 == 0 else - this_move_result)
            else:
                prob_to_win = self.model.predict(np.array([after_move_board.grille]), batch_size=1, verbose=10)[0]
                #self.quiet_print("final prob for {}: {}".format(x, str(result)).rjust(70 - deep * 5))
                result += float(prob_to_win[0])

        #self.quiet_print("final prob for {}: {}".format(previous_x, str(result)).rjust(40 - deep * 5))
        return result


player = GenericKeraXMovePlayer
