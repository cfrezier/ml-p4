import random

from keras.layers import LSTM


class LstmLayer:

    def __init__(self):
        self.layer_size = 1
        self.return_sequences = False
        self.used = True

    def random_return_sequences(self):
        self.return_sequences = random.choice([True, False])
        return self

    def random_layer_size(self):
        self.layer_size = random.choice(list(map(lambda x: 2 ** x, list(range(1, 10)))))
        return self

    def random_full(self):
        return self.random_return_sequences().random_layer_size()

    def build(self, player, output):
        return LSTM(self.layer_size, return_sequences=self.return_sequences)(output)

    def display(self):
        return "LSTM/{}/{}".format(self.layer_size, self.return_sequences)

    def mutate(self):
        mutation = random.random()
        if 0.3 > mutation:
            self.random_layer_size()
        if 0.3 <= mutation < 0.6:
            self.random_return_sequences()
