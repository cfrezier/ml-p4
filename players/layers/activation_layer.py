import random

from keras.layers import Activation


class ActivationLayer:

    def __init__(self):
        self.activation_function = 'relu'
        self.used = True

    def random_activation_function(self):
        self.activation_function = random.choice(['relu', 'elu', 'tanh', 'sigmoid'])
        return self

    def random_full(self):
        return self.random_activation_function()

    def build(self, player, output=None):
        return Activation(self.activation_function)(output)

    def display(self):
        return "Act/{}".format(self.activation_function)

    def mutate(self):
        mutation = random.random()
        if 0.6 > mutation:
            self.random_activation_function()
