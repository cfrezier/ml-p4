import random

from keras.layers import Dense, Flatten


class DenseLayer:

    def __init__(self):
        self.layer_size = 1
        self.activation_function = 'relu'
        self.bias = False
        self.used = True

    def random_activation_function(self):
        self.activation_function = random.choice(['relu', 'elu', 'tanh', 'sigmoid'])
        return self

    def random_bias_use(self):
        self.bias = random.choice([True, False])
        return self

    def random_layer_size(self):
        self.layer_size = random.choice(list(map(lambda x: 2 ** x, list(range(1, 10)))))
        return self

    def random_full(self):
        return self.random_bias_use().random_activation_function().random_layer_size()

    def build(self, player, output=None):

        my_output = output
        if not player.flattened:
            my_output = Flatten()(my_output)
            player.flattened = True

        my_output = Dense(self.layer_size, activation=self.activation_function, use_bias=self.bias)(my_output)
        return my_output

    def display(self):
        return "DNS/{}/{}/{}".format(self.layer_size, self.activation_function, self.bias)

    def mutate(self):
        mutation = random.random()
        if 0.2 > mutation:
            self.random_layer_size()
        if 0.2 <= mutation < 0.4:
            self.random_activation_function()
        if 0.4 <= mutation < 0.6:
            self.random_bias_use()
