import random

from keras.layers import Dropout


class DropoutLayer:

    def __init__(self):
        self.dropout = 0.5
        self.used = True

    def random_dropout(self):
        self.dropout = round(random.uniform(0.01, 0.99), 2)
        return self

    def random_full(self):
        return self.random_dropout()

    def build(self, player, output=None):
        return Dropout(self.dropout)(output)

    def display(self):
        return "Drop/{}".format(self.dropout)

    def mutate(self):
        mutation = random.random()
        if 0.6 > mutation:
            self.random_dropout()
