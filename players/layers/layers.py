import random

from players.layers.activation_layer import ActivationLayer
from players.layers.dense_layer import DenseLayer
from players.layers.dropout_layer import DropoutLayer

layers = [DenseLayer, ActivationLayer, DropoutLayer]


def random_layer():
    return random.choice(layers)()
