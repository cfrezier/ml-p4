# coding=utf-8
from generic_player import Player


class AlwaysPlayer(Player):
    # Joueur qui répond toujours le premier emplacement valide
    def __init__(self, me):
        Player.__init__(self, "Always", me)

    def respond_to(self, board):
        return 1

player = AlwaysPlayer
