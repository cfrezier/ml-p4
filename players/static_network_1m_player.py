# coding=utf-8
import os

import keras
import numpy as np
from keras.layers import Flatten, Dense
from keras.models import Model

from constants import *
from generic_kera_xmove_player import GenericKeraXMovePlayer


class StaticNetworkPlayer(GenericKeraXMovePlayer):
    # Réseau statique
    def __init__(self, me, quiet, name="StaticNetwork", deep=1):
        GenericKeraXMovePlayer.__init__(self, name, me, quiet, deep)
        self.callbacks = None

        self.makeModel()
        self.train()

    def makeModel(self):
        if not os.path.exists("./models"):
            os.mkdir("./models")

        inputs = keras.layers.Input(shape=(HAUTEUR, LARGEUR))

        output = Flatten()(inputs)
        output = Dense(100, activation='relu')(output)
        output = Dense(50, activation='relu')(output)
        output = Dense(20, activation='relu')(output)
        output = Dense(1, activation='relu', use_bias=False)(output)

        self.model = Model(inputs=inputs, outputs=output)

        tbCallBack = keras.callbacks.TensorBoard(log_dir='./log', write_graph=False)
        checkpointCallback = keras.callbacks.ModelCheckpoint(
            './models/model_running.h5_{}_{}'.format(self.name, self.me),
            monitor='val_loss', verbose=0,
            save_best_only=True, save_weights_only=False, mode='auto',
            period=1)
        reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=0.0001)
        self.callbacks = [tbCallBack, checkpointCallback, reduce_lr]

        self.model.compile(loss='mse', optimizer=keras.optimizers.Adam(lr=0.001))

    def train(self):
        boardgames, whowon = self.get_train_data()

        self.model.fit(np.array(boardgames), np.array(whowon), epochs=10, validation_split=0.2, shuffle=True,
                       verbose=0, callbacks=self.callbacks)

        return


player = StaticNetworkPlayer
