# coding=utf-8
import os
import random
import copy

import keras
import numpy as np
from keras import Sequential, Model

from constants import HAUTEUR, LARGEUR
from players.generic_kera_xmove_player import GenericKeraXMovePlayer
from players.layers.dense_layer import DenseLayer
from players.layers.layers import random_layer
from players.optimizers.optimizers import random_optimizer


class GlobalGeneticPlayer(GenericKeraXMovePlayer):
    # Réseau statique avec paramètres aléatoires
    def __init__(self, me, name="Genetic", deep=1, previous=None):
        GenericKeraXMovePlayer.__init__(self, name, me, True, deep)
        self.attacker = True
        self.defender = True
        self.flattened = False
        self.model = None

        # Randomly select number of layers
        layers_nb = random.randint(1, 6)
        self.layers = []
        for i in range(1, layers_nb):
            self.layers.append(random_layer().random_full())

        # last layer
        self.layers.append(DenseLayer().random_activation_function())

        self.optimizer = random_optimizer()
        self.score = 0.0

        if previous is not None:
            self.layers = copy.deepcopy(previous.layers)
            self.optimizer = copy.deepcopy(previous.optimizer)

    def prepare_from_parameters(self):
        self.display()

        # let's go
        self.make_model()
        self.train()
        return self

    def make_model(self):
        if not os.path.exists("./models"):
            os.mkdir("./models")

        self.model = Sequential()

        inputs = keras.layers.Input(shape=(HAUTEUR, LARGEUR))

        output = inputs
        for layer in self.layers:
            try:
                output = layer.build(output=output, player=self)
            except Exception as e:
                self.problem_with_layer(layer, e)
            except ValueError as e:
                self.problem_with_layer(layer, e)
            except TypeError as e:
                self.problem_with_layer(layer, e)
            finally:
                pass

        self.model = Model(inputs=inputs, outputs=output)

        self.model.compile(loss='mse', optimizer=self.optimizer.build(), metrics=['accuracy'])

    def problem_with_layer(self, layer, e):
        print("Problem generating layer {}".format(layer.display()))
        print("Error: ".format(repr(e)))
        self.layers = [l for l in self.layers if not layer == l]

    def train(self):
        boardgames, whowon = self.get_train_data()

        self.model.fit(np.array(boardgames), np.array(whowon), epochs=10, validation_split=0.2, shuffle=True,
                       verbose=0)

        return

    def display(self):
        print("{} | opt: {} | score: {} | layers: {}".format(self.name.rjust(60), self.optimizer.display().rjust(10),
                                                             str(self.score).rjust(10),
                                                             str(list(map(lambda x: x.display(), self.layers)))))

    def mutate(self):
        new_me = GlobalGeneticPlayer(self.me, self.name + "M", self.deep, self)

        # de 1 à 4 mutations (alétoirement choisi)
        nb_mutations = random.randint(1, 4)

        while nb_mutations > 0:
            nb_mutations -= 1

            # Sélection d'un noeud au hasard si besoin
            place = 0 if len(new_me.layers) <= 1 else random.randint(0, len(new_me.layers) - 2)

            # type de mutation aléatoire
            mutation = random.uniform(0, 1)
            if 0.15 > mutation:
                # print("Adding 1 random layer")
                new_me.layers.insert(place, random_layer().random_full())

            if 0.15 <= mutation < 0.25:
                # print("Deleting 1 random layer")
                if len(new_me.layers) > 1 or place > 0:
                    del new_me.layers[place]

            if 0.25 <= mutation < 0.5:
                # print("Randomize 1 random layer")
                if len(new_me.layers) > 1 or place > 0:
                    new_me.layers[place] = random_layer().random_full()

            if 0.5 <= mutation < 0.6:
                # print("Change optimizer")
                new_me.optimizer = random_optimizer()

            if 0.6 <= mutation < 1:
                # print("Mutate existing layers")
                if len(new_me.layers) > 1 or place > 0:
                    new_me.layers[place].mutate()

            # print(new_me.display())

        # but last layer has always a dense size of 1
        if len(new_me.layers) == 0:
            new_me.layers.append(DenseLayer().random_activation_function())

        return new_me

    def breed(self, other_parent):
        # define breeding from 2 parents (randomly keep parameters)
        new_me = GlobalGeneticPlayer(self.me, "-" + self.name + "_" + other_parent.name + "-", self.deep, self)

        # Keep a random number of layers (ignore last dense layer)
        from_nb = min(len(self.layers) - 1, len(other_parent.layers) - 1)
        to_nb = max(len(self.layers) - 1, len(other_parent.layers) - 1)
        layers_nb = random.randint(from_nb, to_nb)

        new_me.layers = []
        # Select each layer from a random parent
        for i in range(0, layers_nb):
            if (0.5 > random.random() or len(self.layers) <= i) and len(other_parent.layers) > i:
                new_me.layers.append(other_parent.layers[i])
            else:
                new_me.layers.append(self.layers[i])

        # Select a random optimizer
        new_me.optimizer = random.choice([self.optimizer, other_parent.optimizer])

        # but last layer has always a dense size of 1
        new_me.layers.append(DenseLayer().random_activation_function())

        return new_me


player = GlobalGeneticPlayer
