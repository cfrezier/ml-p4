# coding=utf-8
import random

from generic_player import Player


class RandomPlayer(Player):
    # Joueur qui répond aléatoirement
    def __init__(self, me, quiet):
        Player.__init__(self, "Random", me, quiet)
        random.seed()

    def respond_to(self, board):
        moves = board.valid_moves()
        return moves[random.randint(0, len(moves) - 1)]


player = RandomPlayer
