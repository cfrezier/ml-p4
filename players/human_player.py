# coding=utf-8
from constants import *
from generic_player import Player


class HumanPlayer(Player):
    # Joueur qui attends l'entrée de l'utilisateur pour répondre (jeu humain)
    def __init__(self, me, quiet):
        Player.__init__(self, "Human", me, quiet)

    def respond_to(self, board):
        try:
            board.exportPNG("./current_game.png")
        except:
            pass

        inputed = input("Dans quelle colonne voulez-vous jouer (1-{}) ?".format(LARGEUR))

        if not int(inputed) in range(1, LARGEUR + 1):
            return self.respond_to(board)

        return int(inputed)


player = HumanPlayer
