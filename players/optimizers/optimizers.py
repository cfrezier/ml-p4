import random

import keras


class OptimizerAdamax:
    def __init__(self):
        pass

    def build(self):
        return 'adamax'

    def display(self):
        return 'adamax'


class OptimizerRmsProp:
    def __init__(self):
        pass

    def build(self):
        return 'rmsprop'

    def display(self):
        return 'rmsprop'


class OptimizerAdam:
    def __init__(self):
        pass

    def build(self):
        return keras.optimizers.Adam(lr=0.001)

    def display(self):
        return 'adam'


class OptimizerAdagrad:
    def __init__(self):
        pass

    def build(self):
        return 'adagrad'

    def display(self):
        return 'adagrad'


optimizers = [OptimizerAdam, OptimizerAdamax, OptimizerRmsProp, OptimizerAdagrad]


def random_optimizer():
    return random.choice(optimizers)()
