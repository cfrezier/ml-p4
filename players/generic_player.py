# coding=utf-8
import os

from constants import *


class Player:
    # Classe incluant les fonctions de base pour un joueur
    def __init__(self, name, me, quiet=False):
        # Name
        self.name = name
        self.me = me
        self.quiet = quiet

    def __str__(self):
        return "{0}".format(self.name)

    def respond_to(self, board):
        return -1

    def saveModel(self):
        return

    def quiet_print(self, str):
        if not self.quiet:
            print(self.name, ": ", str)

    def get_train_data(self):
        datadir = "./dataset/"
        if not os.path.exists(datadir):
            os.mkdir(datadir)
        game_result_list = os.listdir(datadir)
        boardgames = []
        whowon = []

        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

        for result in game_result_list:
            with open(datadir + result, "r") as fichier:
                while 1:
                    readline = fichier.readline()
                    if not readline: break
                    board, score = readline.split(";")

                    new_board = []
                    for x in range(HAUTEUR):
                        line = []
                        for y in range(LARGEUR):
                            line.append(board[y + x * LARGEUR])
                        new_board.append(line)

                    boardgames.append(new_board)
                    whowon.append(score)

        return boardgames, whowon
