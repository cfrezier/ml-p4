# coding=utf-8


class DummyGeneticPlayer:
    # Réseau statique avec paramètres aléatoires
    def __init__(self, me, name="DummyGenetic", deep=3, previous=None):
        self.me = me
        self.name = name
        pass

    def prepare_from_parameters(self):
        return self

    def make_model(self):
        pass

    def train(self):
        pass

    def display(self):
        print("dummy_display: ", self.name)

    def mutate(self):
        return DummyGeneticPlayer(self.me, self.name + "_M")

    def breed(self, other_parent):
        return DummyGeneticPlayer(self.me, self.name + "_" + other_parent.name)

    def saveModel(self):
        pass


player = DummyGeneticPlayer
