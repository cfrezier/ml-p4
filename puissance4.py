# coding=utf-8
import random
import sys
import warnings

sys.path.insert(0, './players/')

warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")

from constants import *
from grille import Grille

# initialise l'ensemble
if len(sys.argv) == 4:
    quiet = True
else:
    quiet = False


def quiet_print(str):
    if not quiet:
        print(str)


# verifie que l'on a bien deux joueurs
if len(sys.argv) < 3:
    quiet_print("Le programme n'a pas été lancé correctement")
    quiet_print("exemple : ./puissance_4.py joueur1 joueur2")
else:
    quiet_print("Pret pour la bataille !")
    quiet_print("Tirage au sort du premier joueur...")
    random.seed()

    choix = random.randint(0, 1)

    mod_player1_name = sys.argv[choix + 1].replace(".py", "")
    mod_player2_name = sys.argv[(choix + 1) % 2 + 1].replace(".py", "")

    mod_player1 = __import__(mod_player1_name, fromlist=['player'])
    mod_player2 = __import__(mod_player2_name, fromlist=['player'])

    player1 = getattr(mod_player1, 'player')(1, quiet)
    player2 = getattr(mod_player2, 'player')(2, quiet)

    quiet_print("le joueur 1 sera : {}".format(player1.name))
    quiet_print("le joueur 2 sera : {}".format(player2.name))

save = True
grille = Grille(hauteur=HAUTEUR, largeur=LARGEUR, fichier=FILENAME, quiet=quiet)
fin = 0
while not fin:
    if not quiet:
        grille.afficher()
    colonne = -1

    if grille.etape % 2 == 1:
        # le joueur 1 joue
        current_player = player1
        current_J = J1
    else:
        current_player = player2
        current_J = J2

    try:
        colonne = current_player.respond_to(grille)
        resu = grille.jouer(current_J, int(colonne))
    except:
        quiet_print("Input incorrect !")
        resu = 1

    if resu:
        # Le joueur a mal joué = défaite
        print("Le joueur {} a joué un coup impossible (colonne {})".format(2 - grille.etape % 2, colonne))
        fin = -(grille.etape % 2 + 1)
        save = False
    else:
        fin = grille.test_victoire()
    # passe a l'etape suivante
    grille.etape += 1
    grille.remember_board()

# Fin de la partie
if not quiet:
    grille.afficher()
    if fin == 42:
        quiet_print("---- Egalite parfaite ! ----\n")
    elif fin == 1 or fin == -1:
        quiet_print("Le Joueur 1 ({}) a gagné !".format(player1))
    elif fin == 2 or fin == -2:
        quiet_print("Le Joueur 2 ({}) a gagné !".format(player2))
    else:
        quiet_print("Oupsie !")
        save = False

quiet_print("Score de la partie en tant que joueur 1: {}".format(grille.score(fin)))

# Ajout de toutes les étapes au dataset
if save is True:
    grille.output_boards(grille.score(fin))
