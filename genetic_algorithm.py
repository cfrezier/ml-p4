# coding=utf-8

import random

from constants import *
from grille import Grille


class GeneticAlgorithm:
    def __init__(self, nombre_matches=10, pourcentage_meilleurs=0.40, pourcentage_enfants=0.4,
                 conservation_aleatoire=0.1, mutation=0.1):
        self.nombre_matches = nombre_matches
        self.pourcentage_meilleurs = pourcentage_meilleurs
        self.pourcentage_enfants = pourcentage_enfants
        self.conservation_aleatoire = conservation_aleatoire
        self.mutation = mutation
        self.timer = None

    def match(self, player_1, player_2):
        grille = Grille(hauteur=HAUTEUR, largeur=LARGEUR, fichier=FILENAME, quiet=True)

        player_1.me = 1
        player_2.me = 2

        fin = 0
        while not fin:
            if grille.etape % 2 == 1:
                # le joueur 1 joue
                current_player = player_1
                current_J = J1
            else:
                current_player = player_2
                current_J = J2

            try:
                colonne = current_player.respond_to(grille)
                resu = grille.jouer(current_J, int(colonne))
            except:
                resu = 1

            if resu:
                # Le joueur a mal joué = défaite
                fin = -(grille.etape % 2 + 1)
            else:
                fin = grille.test_victoire()
            # passe a l'etape suivante
            grille.etape += 1
            grille.remember_board()

        return grille.score(fin), grille

    def evolve(self, players, generate_new_population):
        """Evolve a population of players."""
        boards = []

        # Reinit scores
        for player in players:
            player.score = 0.0

        print("Simulation des parties...")
        # Get scores for each network.
        current_it = 0
        total_it_nb = (len(players)) * (len(players) - 1) * self.nombre_matches
        for i in range(0, len(players)):
            for j in range(0, len(players)):
                if not i == j:
                    count = self.nombre_matches
                    while count > 0:
                        self.printProgressBar(current_it, total_it_nb)
                        score, board = self.match(players[i], players[j])
                        players[i].score += score
                        players[j].score -= score
                        count -= 1
                        current_it += 1
                        boards.append([score, board])

        self.printProgressBar(current_it, total_it_nb)
        print("Parties jouées:", current_it, "/", total_it_nb)

        # Sort players by total score
        players_sorted = sorted(players, key=lambda p: p.score, reverse=True)

        print("Résultat de la simulation:")
        for p in players_sorted:
            p.display()

        print("Sauvegarde des {} simulations...".format(len(boards)))
        # Save boards
        for b in boards:
            score = b[0]
            board = b[1]
            board.output_boards(score, "./evolving_boards.history")

        if generate_new_population:
            # Get the number we want to keep for the next gen.
            a_conserver = max(1, int(len(players_sorted) * self.pourcentage_meilleurs))

            # The parents are every network we want to keep.
            print("Conservation des {} meilleurs individus...".format(a_conserver))
            parents_next_generation = players_sorted[:a_conserver]
            discarded = players_sorted[a_conserver:]

            # Randomly keep some unretained players
            a_garder = max(1, int(len(players_sorted) * self.conservation_aleatoire))
            keep = []
            print("Conservation de {} autres individus aléatoires...".format(a_garder))
            while a_garder > len(keep):
                to_keep = random.randint(0, len(discarded) - 1)
                keep.append(discarded[to_keep])
            parents_next_generation.extend(keep)

            # Randomly mutate
            a_muter = max(1, int(len(players_sorted) * self.mutation))
            mutated = []
            print("Muter {} individus aléatoirement...".format(a_muter))
            while a_muter > len(mutated):
                to_mutate = random.randint(0, len(parents_next_generation) - 1)
                mutated.append(parents_next_generation[to_mutate].mutate().prepare_from_parameters())
            parents_next_generation.extend(mutated)

            # Randomly generate children
            a_croiser = max(1, int(self.pourcentage_enfants * len(players)))
            children = []
            print("Effectuer {} croisements...".format(a_croiser))
            while a_croiser > len(children):

                # Randomly select parents
                parent_1 = random.randint(0, len(parents_next_generation) - 1)
                parent_2 = random.randint(0, len(parents_next_generation) - 1)

                # Assuming they aren't the same network...
                if parent_1 != parent_2:
                    parent_1 = parents_next_generation[parent_1]
                    parent_2 = parents_next_generation[parent_2]

                    children.append(parent_1.breed(parent_2).prepare_from_parameters())
            parents_next_generation.extend(children)

            print("Nouvelle population constituée de {} individus".format(len(parents_next_generation)))

            return parents_next_generation
        else:
            return players_sorted

    def printProgressBar(self, iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filled_length = int(length * iteration // total)
        bar = fill * filled_length + '-' * (length - filled_length)
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
        # Print New Line on Complete
        if iteration == total:
            print()
