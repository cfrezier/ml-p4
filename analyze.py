# coding=utf-8
content = []
nb = 0
the_score = dict()
with open("dataset/boards.history", "r") as fichier:
    while 1:
        readline = fichier.readline()
        if not readline: break
        board, score = readline.split(";")
        content.append(board)
        nb += 1
        if score in the_score.keys():
            the_score[score] += 1
        else:
            the_score[score] = 1

print("Nombre de boards: {}".format(nb))
print("Nombre de boards différentes: {}".format(len(set(content))))
print("                        soit: {} %".format(round(float(len(set(content))) / float(nb) * 100.0, 2)))
print("Répartition des scores par board: " + str(the_score))
