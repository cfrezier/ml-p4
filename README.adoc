= Machine-learning Puissance 4

== Dépendances

Tensorflow = 1.4.1
Keras = 2.1.2
Vous pouvez essayer avec d'autres versions, mais j'ai souvent eu des erreurs liées à ceci sinon: https://github.com/kamalkraj/Named-Entity-Recognition-with-Bidirectional-LSTM-CNNs/issues/6
Ou sinon, il faut enlever les réseaux type LSTM.

== Commandes
- Lancement d'une partie entre 2 joueurs :
----
python puissance4.py random_player human_player
----
- Lancement en mode discret:
----
python puissance4.py random_player human_player a
----
- Analyser basiquement votre dataset:
----
python analyze.py
----
- Créer un gif animé d'une partie:
----
python vizualize.py ./dataset/last_game.history
----
- Créer un gif animé d'une partie, en laissant tous les png intermédiaires:
----
python vizualize.py ./dataset/last_game.history a
----

== Autres infos générales
- A la racine, tout ce qui finit par "player.py" est un joueur
- Un joueur est une classe héritant de Player, et qui déclare dans son fichier une variable `player` qui contient cette même classe (pour permettre le chargement dynamique)
- Si le fichier commence par "generic" c'est ce qui correspondrait à une classe abstraite
- Le répertoire `test` contient des tests unitaires

== Infos IA
- Toutes les parties sont concaténées dans `dataset/boards.history` à la fin de chaque exécution
- `dataset/last_game.history` est la dernière partie jouée
- Une partie échouée (i.e. quelqu'un réponds un coup impossible, ...) n'est pas enregistrée
- Les répertoires `log` et `models` sont utilisés par keras
- Les IA héritant de `generic_kera_xmove_player` portent une partie d'algo de finisseur/défenseur qui n'est pas du learning.
- `get_last_game.py` permet de récupérer la dernière partie stockée dans `boards.history`
Pour les désactiver, changer les propriétés:
----
player.finisher = False
player.defender = False
----
- Pour lancer une IA, il convient tout d'abord qu'elle dispose d'au moins une partie pour pouvoir réaliser son entrainement. (Une partie random/random conviendra parfaitement)

