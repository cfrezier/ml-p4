# coding=utf-8
import unittest

from mock import Mock
from players.generic_kera_xmove_player import GenericKeraXMovePlayer

from grille import Grille


class GenericKeraXMovePlayerTest(unittest.TestCase):
    """Test case utilisé pour tester les fonctions du module 'GenericKeraXMovePlayerTest'."""

    def returnMiddleproba(self, batch_size=None, verbose=None, a3=None, a4=None):
        return [0.5]

    def test_defender(self):
        """Test le fonctionnement de la fonction 'random.choice'."""
        grille = Grille(4, 6, "./defender.txt", False)
        grille.importer()

        player = GenericKeraXMovePlayer("test_generic", 1, False, 2)
        player.model = Mock()
        player.model.predict = Mock(return_value=[[0.0001]])

        self.defender = True

        self.assertEqual(player.evaluate_next_moves_at_deep(grille, 1, 2), 100.0, "Doit défendre correctement")

    def test_attacker(self):
        """Test le fonctionnement de la fonction 'random.choice'."""
        grille = Grille(4, 6, "./attacker.txt", False)
        grille.importer()

        player = GenericKeraXMovePlayer("test_generic", 1, False, 2)
        player.model = Mock()
        player.model.predict = Mock(return_value=[[0.0001]])

        self.finisher = True

        self.assertEqual(player.evaluate_next_moves_at_deep(grille, 1, 2), 100.0, "Doit attaquer correctement")
