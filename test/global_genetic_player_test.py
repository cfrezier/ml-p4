# coding=utf-8
import unittest

from players.global_genetic_player import GlobalGeneticPlayer
from players.optimizers.optimizers import OptimizerAdam, OptimizerAdagrad


class DenseGeneticPlayerTest(unittest.TestCase):

    def test_mutation(self):
        player = GlobalGeneticPlayer(1, "GPtest")

        for i in range(1, 100):
            player = player.mutate()

            self.assertEqual(player.layers[len(player.layers) - 1].layer_size, 1,
                             "La derniere couche doit forcement avoir une taille de 1")

        player.display()

    def test_breed(self):
        player1 = GlobalGeneticPlayer(1, "GPtest1")
        player2 = GlobalGeneticPlayer(1, "GPtest2")

        player1.optimizer = OptimizerAdam()
        player2.optimizer = OptimizerAdagrad()

        childs = []
        for i in range(1, 30):
            childs.append(player1.breed(player2))

        player1.display()
        player2.display()

        for child in childs:
            child.display()

            self.assertEqual(child.name, "-GPtest1_GPtest2-", "nom d'enfant portant les parents")
            self.assertIn(child.optimizer.display(), ["adam", "adagrad"], "on doit garder un des optimizers parents")
            self.assertEqual(child.layers[len(child.layers) - 1].layer_size, 1,
                             "La derniere couche doit forcement avoir une taille de 1")
