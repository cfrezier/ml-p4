# coding=utf-8
import copy
import hashlib
import os

from PIL import Image, ImageDraw

from constants import *


class Grille:

    def __init__(self, hauteur, largeur, fichier=None, quiet=False):
        self.hauteur = hauteur
        self.largeur = largeur
        self.fichier = fichier
        self.etape = 1
        self.grille = [[EMPTY for x in range(largeur)] for y in range(hauteur)]
        self.taille = largeur * hauteur
        self.history = []
        self.quiet = quiet

    def test_victoire(self):
        """
        retourne 0 si égalité
        1 si J1 gagne
        2 si J2 gagne
        42 si égalité parfaite (toute case prise sans possibilité de jouer)
        """
        for y in range(0, self.hauteur):
            for x in range(0, self.largeur):
                player = self.grille[y][x]

                if player == '1' or player == '2':

                    if x + 3 < self.largeur and \
                            player == self.grille[y][x + 1] and \
                            player == self.grille[y][x + 2] and \
                            player == self.grille[y][x + 3]:
                        return int(player)

                    if y + 3 < self.hauteur:

                        if player == self.grille[y + 1][x] and \
                                player == self.grille[y + 2][x] and \
                                player == self.grille[y + 3][x]:
                            return int(player)

                        if x + 3 < self.largeur and \
                                player == self.grille[y + 1][x + 1] and \
                                player == self.grille[y + 2][x + 2] and \
                                player == self.grille[y + 3][x + 3]:
                            return int(player)

                    if y - 3 >= 0 and x + 3 < self.largeur and \
                            player == self.grille[y - 1][x + 1] and \
                            player == self.grille[y - 2][x + 2] and \
                            player == self.grille[y - 3][x + 3]:
                        return int(player)

        if self.etape == self.taille:
            return 42

        return 0  # Personne ne gagne pour le moment

    def hash(self):
        """ Retourne le hash du contenu en parametre """
        hacheur = hashlib.md5()
        hacheur.update(self.grille)
        return hacheur.digest()

    def exporter(self, fichier=None):
        """ Exporte la grille dans un fichier """
        if fichier is not None:
            f = fichier
        elif self.fichier is not None:
            f = self.fichier
        else:
            return 1

        try:
            contenu = ""
            for y in range(self.hauteur - 1, -1, -1):
                for x in range(0, self.largeur):
                    contenu += self.grille[y][x]
                contenu += "\n"
            with open(f, "w") as fichier:
                fichier.write(contenu)
            return 0
        except:
            print("Problème d'ouverture du fichier")
            return 1

    def remember_board(self):
        """ Sauvegarde le board pour pouvoir l'extraire plus tard """

        contenu = ""
        for y in range(self.hauteur - 1, -1, -1):
            for x in range(0, self.largeur):
                contenu += self.grille[y][x]

        self.history.append(contenu)
        return 0

    def output_boards(self, score, filename="dataset/boards.history"):
        if not os.path.exists("./dataset"):
            os.mkdir("./dataset")

        contenu = ""
        for board in self.history:
            contenu += "{};{}\n".format(board, score)

        try:
            if os.path.exists(filename):
                append_write = 'a'  # append if already exists
            else:
                append_write = 'w'
            with open(filename, append_write) as fichier:
                fichier.write(contenu)
            with open("dataset/last_game.history", "w") as fichier:
                fichier.write(contenu)
        except:
            print("Problème d'ouverture du fichier")
            return 1

        return 0

    def exportPNG(self, fichier):
        """ Exporte la grille dans un png """
        if fichier is None:
            return 1
        taille = JETON_SIZE + JETON_OFFSET * 2
        rayon = JETON_SIZE / 2
        largeur = taille * self.largeur + taille / 2
        hauteur = taille * self.hauteur + taille / 2
        image = Image.new("RGB", (largeur, hauteur), BLEU)
        dessinateur = ImageDraw.Draw(image)

        for y in range(0, HAUTEUR):
            for x in range(0, LARGEUR):
                if self.grille[y][x] == J1:
                    couleur = JAUNE
                elif self.grille[y][x] == J2:
                    couleur = ROUGE
                else:
                    couleur = NOIR

                if couleur is not None:
                    centre_x = JETON_OFFSET + taille / 2 + taille * x
                    centre_y = hauteur - taille / 2 - JETON_OFFSET - taille * y
                    dessinateur.ellipse(
                        [(centre_x - rayon, centre_y - rayon),
                         (centre_x + rayon, centre_y + rayon)],
                        couleur, NOIR
                    )
        image.save(fichier)

    def importer(self, fichier=None):
        """ Importe la grille depuis un fichier """
        if fichier is not None:
            f = fichier
        elif self.fichier is not None:
            f = self.fichier
        else:
            return 0

        try:
            with open(f, "r") as fichier:
                contenu = fichier.read()

            y = 0;  # self.hauteur
            x = 0;  # largeur
            for car in contenu:
                try:
                    if (car == '\n'):
                        y += 1
                        x = 0
                    else:
                        self.grille[self.hauteur - y - 1][x] = car
                        x += 1
                except:
                    print("Souci dans la recuperation")
                    return 0
            return 1
        except:
            print("Problème d'ouverture du fichier")
            return 0

    def afficher(self, filename=None):
        """ Affiche la grille dans la console """
        contenu = ""
        for y in range(self.hauteur - 1, -1, -1):
            for x in range(0, self.largeur):
                contenu += self.grille[y][x].replace("0", "_").replace("1", "X").replace("2", "O")
            contenu += "\n"
        for x in range(1, self.largeur + 1):
            contenu += str(x)
        contenu += "\n"

        print(contenu)

    def jouer(self, joueur, colonne):
        """ Joue un pion dans une colonne """
        colonne -= 1  # offset
        if colonne > self.largeur - 1:
            colonne = self.largeur - 1
        elif colonne < 0:
            colonne = 0

        for y in range(0, self.hauteur):
            if self.grille[y][colonne] == EMPTY:
                self.grille[y][colonne] = joueur
                return 0  # on a reussi a jouer dans la colonne

        return 1  # on a pas pu jouer dans cette colonne (pleine)

    def simuler(self, joueur, colonne):
        """ Simule un pion dans une colonne """
        colonne -= 1  # offset
        if colonne > self.largeur - 1:
            colonne = self.largeur - 1
        elif colonne < 0:
            colonne = 0

        temp_grille = copy.deepcopy(self.grille)

        for y in range(0, self.hauteur):
            if temp_grille[y][colonne] == EMPTY:
                temp_grille[y][colonne] = str(joueur)
                break

        result = Grille(self.hauteur, self.largeur, None, True)
        result.grille = temp_grille
        return result

    def debug_set(self, x, y, value):
        self.grille[x][y] = value

    def score(self, fin):
        value = 64 - self.etape
        if fin == 42:
            result = 0
        elif fin == 1:
            result = value
        else:
            result = -value
        return float(result) / 64.0

    def valid_moves(self):
        valid_moves = []
        for x in range(self.largeur):
            if self.grille[self.hauteur - 1][x] == '0':
                valid_moves.append(x + 1)
        return valid_moves

    def quiet_print(self, str):
        if not self.quiet:
            print(str)

    def invert(self):
        temp_grille = copy.deepcopy(self.grille)
        result = Grille(self.hauteur, self.largeur, None, True)
        for y in range(0, self.hauteur):
            for x in range(0, self.largeur):
                actual = temp_grille[y][x]
                temp_grille[y][x] = '2' if actual == '1' else '1' if actual == '2' else actual
        result.grille = temp_grille
        return result
