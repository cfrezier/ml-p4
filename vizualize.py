#!python
# -*- coding: utf-8 -*-

import glob
import multiprocessing
import os
import subprocess
import sys

from PIL import Image, ImageDraw

from constants import *

if len(sys.argv) < 1:
    print("Le programme n'a pas ete lance correctement")
    print("exemple : ./vizualize.py partie.history")


def grille(board, x, y):
    return board[x + (HAUTEUR - y - 1) * LARGEUR]


line_num = 0
split = sys.argv[1].split(".")
ext = split[len(split) - 1]
complete_basename = ""
for i in range(0, len(split) - 1):
    complete_basename += str(split[i]) + "."
complete_basename = complete_basename[:-1]
basename = complete_basename.split("/")
basename = basename[len(basename) - 1]

with open(sys.argv[1], "r") as fichier:
    while 1:
        readline = fichier.readline()
        if not readline: break

        board, score = readline.split(";")

        taille = JETON_SIZE + JETON_OFFSET * 2
        rayon = JETON_SIZE / 2
        largeur = int(taille * LARGEUR + taille / 2)
        hauteur = int(taille * HAUTEUR + taille / 2)
        image = Image.new("RGB", (largeur, hauteur), BLEU)

        dessinateur = ImageDraw.Draw(image)

        for y in range(0, HAUTEUR):
            for x in range(0, LARGEUR):
                if grille(board, x, y) == J1:
                    couleur = JAUNE
                elif grille(board, x, y) == J2:
                    couleur = ROUGE
                else:
                    couleur = NOIR

                if couleur is not None:
                    centre_x = JETON_OFFSET + taille / 2 + taille * x
                    centre_y = hauteur - taille / 2 - JETON_OFFSET - taille * y
                    dessinateur.ellipse(
                        [(centre_x - rayon, centre_y - rayon),
                         (centre_x + rayon, centre_y + rayon)],
                        couleur, NOIR
                    )
        image.save("{}_{:03d}.png".format(basename, line_num))
        line_num += 1
fichier.close()

temporary_files = "{}_*.png".format(basename)
subprocess.call(["convert", "-delay", "100", temporary_files, "./{}.gif".format(basename)])

if not len(sys.argv) > 2:
    p = multiprocessing.Pool(4)
    p.map(os.remove, glob.glob(temporary_files))
