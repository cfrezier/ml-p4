# coding=utf-8
from PIL import ImageColor

EMPTY = "0"
J1 = "1"
J2 = "2"

JETON_SIZE = 20
JETON_OFFSET = 2

JAUNE = ImageColor.getrgb("#ffff00")
ROUGE = ImageColor.getrgb("#ff0000")
BLEU = ImageColor.getrgb("#0000ff")
NOIR = ImageColor.getrgb("#000000")

LARGEUR = 7
HAUTEUR = 6
FILENAME = "grille.txt"