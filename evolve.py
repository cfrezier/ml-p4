# coding=utf-8
import os
import sys
import time

from players.global_genetic_player import GlobalGeneticPlayer

sys.path.insert(0, './players/')

from genetic_algorithm import GeneticAlgorithm

target_generations = 10
base_population = 20
generation = 0
population = []

# Initial random population
print("Génération de la population initiale (taille: {})...".format(base_population))
for i in range(0, base_population):
    population.append(GlobalGeneticPlayer(1, "GP{}".format(i)).prepare_from_parameters())

evolution = GeneticAlgorithm(nombre_matches=20, pourcentage_meilleurs=0.40, pourcentage_enfants=0.4,
                             conservation_aleatoire=0.1, mutation=0.1)

global_start = time.time()

while generation < target_generations:
    generation += 1

    generation_start = time.time()

    print("******************************")
    print("Calcul de la génération {}...".format(generation))

    population = evolution.evolve(population, generation != target_generations)

    generation_time = time.time() - generation_start
    global_time = time.time() - global_start
    print("Temps de la génération: {}h".format(round(generation_time / 3600, 4)))
    print("Temps depuis le début: {}h".format(round(global_time / 3600, 4)))

    print("Fin de la génération {}.".format(generation))

    print("******************************")
    for p in population:
        p.score = 0.0
        p.saveModel()
        p.display()

    os.rename("evolving_boards.history",
              "dataset/evolving_boards_g{}_{}.history".format(generation, time.strftime("%Y%m%d-%H%M%S")))
